package xyz.p4t.plsdrawmyoc;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.SeekBar;

import com.github.danielnilsson9.colorpickerview.view.ColorPickerView;

class BrushDialog {
    private AlertDialog dialog;
    private float lwidth;
    private int lcolor;

    BrushDialog(Context ctx, final BrushDialog.OnOKListener listener, final float width, final int color) {
        LayoutInflater factory;
        View view;
        final ColorPickerView color_picker;
        final BrushView brush_view;
        final SeekBar bar;

        factory = LayoutInflater.from(ctx);
        view = factory.inflate(R.layout.brush_settings, null);

        lcolor = color;
        lwidth = width;

        brush_view = (BrushView) view.findViewById(R.id.brush_preview);
        brush_view.set_width(width);
        brush_view.set_color(color);

        color_picker = (ColorPickerView) view.findViewById(R.id.color_picker);
        color_picker.setColor(color);
        color_picker.setOnColorChangedListener(new ColorPickerView.OnColorChangedListener() {
            @Override
            public void onColorChanged(int newColor) {
                brush_view.set_color(newColor);
            }
        });

        bar = (SeekBar) view.findViewById(R.id.brush_size);
        bar.setProgress(width_to_progress(width));
        bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                brush_view.set_width(progress_to_width(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                //pass
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                //pass
            }
        });

        dialog = new AlertDialog.Builder(ctx)
                .setTitle(R.string.brush)
                .setView(view)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        lwidth = progress_to_width(bar.getProgress());
                        lcolor = color_picker.getColor();
                        listener.onOK(lwidth, lcolor);
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        brush_view.set_color(lcolor);
                        brush_view.set_width(lwidth);

                        bar.setProgress(width_to_progress(lwidth));
                        color_picker.setColor(lcolor);
                    }
                }).create();
    }

    private float progress_to_width(int progress) {
        return (float) progress * 55 / 100 + 5;
    }

    private int width_to_progress(float width) {
        return Math.round((width - 5) * 100 / 55);
    }

    void show() {
        dialog.show();
    }

    interface OnOKListener {
        void onOK(float width, int color);
    }
}
