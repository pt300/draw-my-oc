package xyz.p4t.plsdrawmyoc;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

public class BrushView extends View {
    public Paint paint = new Paint();

    public float width = 20;
    float x;
    float y;

    public BrushView(Context context) {
        super(context);
    }

    public BrushView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BrushView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public BrushView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }


    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setStyle(Paint.Style.FILL);
        paint.setStrokeWidth(1);

        x = (float) w / 2;
        y = (float) h / 2;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawCircle(x, y, width, paint);
    }

    public void set_width(float width) {
        this.width = width;
        invalidate();
    }

    public void set_color(int color) {
        paint.setColor(color);
        invalidate();
    }


}
