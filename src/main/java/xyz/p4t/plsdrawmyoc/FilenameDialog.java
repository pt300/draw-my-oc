package xyz.p4t.plsdrawmyoc;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

class FilenameDialog {

    private AlertDialog dialog;
    private EditText edit_text;

    FilenameDialog(Context ctx, final OnOKListener listener) {
        LayoutInflater factory;
        View view;

        factory = LayoutInflater.from(ctx);
        view = factory.inflate(R.layout.filename, null);

        edit_text = ((EditText) view.findViewById(R.id.filename));
        dialog =  new AlertDialog.Builder(ctx)
                .setTitle(R.string.filename)
                .setView(view)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        listener.onOK(edit_text.getText().toString());
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //pass
                    }
                })
                .create();
    }

    void clear() {
        edit_text.getText().clear();
    }

    void show() {
        dialog.show();
    }

    interface OnOKListener {
        void onOK(String name);
    }
}
