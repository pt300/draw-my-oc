package xyz.p4t.plsdrawmyoc;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.Environment;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class DrawView extends View {
    private Canvas canvas;
    private Bitmap bitmap;
    private Paint paint;

    private Paint copy_paint;
    private Path temp_path;

    private Paint clear_paint;

    private int width, height;

    public DrawView(Context context) {
        super(context);
        init();
    }

    public DrawView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DrawView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public DrawView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    void init() {
        paint = new Paint();
        paint.setColor(Color.BLUE);
        paint.setAntiAlias(true);
        paint.setStrokeWidth(20);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeCap(Paint.Cap.ROUND);

        clear_paint = new Paint();
        clear_paint.setColor(Color.parseColor("#EEEEEE"));
        clear_paint.setStyle(Paint.Style.FILL);

        copy_paint = new Paint(Paint.DITHER_FLAG);
        temp_path = new Path();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if (bitmap == null) {
            width = w;
            height = h;

            bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
            canvas = new Canvas(bitmap);

            clear();
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawBitmap(bitmap, 0, 0, copy_paint);
        canvas.drawPath(temp_path, paint);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                temp_path.moveTo(event.getX(), event.getY());
                break;
            case MotionEvent.ACTION_MOVE:
                for (int i = 0; i < event.getHistorySize(); i++) {
                    /*
                    Gives smoother paths
                    */
                    temp_path.lineTo(event.getHistoricalX(i), event.getHistoricalY(i));
                }
                temp_path.lineTo(event.getX(), event.getY());
                break;
            case MotionEvent.ACTION_UP:
                canvas.drawPath(temp_path, paint);
                temp_path.reset();
                break;
            default:
                return false;
        }

        invalidate();
        return true;
    }

    public void clear() {
        canvas.drawRect(0, 0, width, height, clear_paint);
        temp_path.reset();
        invalidate();
    }

    public void set_width(float width) {
        paint.setStrokeWidth(width);
    }

    public void set_color(int color) {
        paint.setColor(color);
    }

    public void save_image(String name) throws IOException {
        String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).getAbsolutePath();
        File output = new File(path + "/" + name + ".png");
        FileOutputStream ostream;

        output.createNewFile();
        ostream = new FileOutputStream(output);
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, ostream);
        ostream.flush();
        ostream.close();

    }
}
