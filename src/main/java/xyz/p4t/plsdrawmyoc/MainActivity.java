package xyz.p4t.plsdrawmyoc;

/*
https://twitter.com/pt300vec
 */

import android.graphics.Color;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.io.IOException;

public class MainActivity extends Activity {

    DrawView draw_view;
    BrushDialog brush_dialog;
    FilenameDialog filename_dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        draw_view = (DrawView) findViewById(R.id.draw_surface);
        draw_view.set_width(20.0f);
        draw_view.set_color(Color.BLUE);

        brush_dialog = new BrushDialog(this, new BrushDialog.OnOKListener() {
            @Override
            public void onOK(float width, int color) {
                draw_view.set_width(width);
                draw_view.set_color(color);
            }
        }, 20.0f, Color.BLUE);

        filename_dialog = new FilenameDialog(this, new FilenameDialog.OnOKListener() {
            @Override
            public void onOK(String name) {
                try {
                    draw_view.save_image(name);
                    Toast.makeText(getApplicationContext(), R.string.saved, Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), getString(R.string.error) + "\n" + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_clear:
                draw_view.clear();
                filename_dialog.clear();
                break;
            case R.id.brush_settings:
                brush_dialog.show();
                break;
            case R.id.action_save:
                filename_dialog.show();
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
